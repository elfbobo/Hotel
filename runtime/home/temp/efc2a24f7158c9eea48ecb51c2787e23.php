<?php /*a:3:{s:52:"G:\phpstudy_pro\WWW\tp\view\home\handover\index.html";i:1604901009;s:51:"G:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"G:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
<script src="/static/plug/html2canvas.js"></script>
<body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a>
                    <cite>交班管理</cite>
                </a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid" id="seho">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-body ">


                            <div class="layui-col-md12">
                                <div class="layui-card">
                                    <div class="layui-card-header">数据统计</div>
                                    <div class="layui-card-body ">
                                        <ul class="layui-row layui-col-space10 layui-this x-admin-carousel x-admin-backlog">
                                            <li class="layui-col-md2 layui-col-xs6">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>总收入</h3>
                                                    <p>
                                                        <cite>
                                                            <?php echo htmlentities($res['home_subscribe_msg'] + $res['member_record'] + $res['move'] + $res['goods_shop']); ?>元
                                                        </cite>
                                                    </p>
                                                </a>
                                            </li>
                                            <li class="layui-col-md2 layui-col-xs6">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>总支出</h3>
                                                    <p>
                                                        <cite>12</cite></p>
                                                </a>
                                            </li>
                                            <li class="layui-col-md2 layui-col-xs6">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>新增会员</h3>
                                                    <p>
                                                        <cite>99</cite></p>
                                                </a>
                                            </li>
                                            <li class="layui-col-md2 layui-col-xs6">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>商品数</h3>
                                                    <p>
                                                        <cite>67</cite></p>
                                                </a>
                                            </li>
                                            <li class="layui-col-md2 layui-col-xs6">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>文章数</h3>
                                                    <p>
                                                        <cite>67</cite></p>
                                                </a>
                                            </li>
                                            <li class="layui-col-md2 layui-col-xs6 ">
                                                <a href="javascript:;" class="x-admin-backlog-body">
                                                    <h3>文章数</h3>
                                                    <p>
                                                        <cite>6766</cite></p>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="layui-col-sm6 layui-col-md3" style="border-right-style: dotted;">
                                <div class="layui-card">
                                    <div class="layui-card-header">
                                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">预交班基本信息</span></div>
                                    <div class="layui-card-body  ">
                                        <p class="layuiadmin-big-font">
                                            班次时间：<?php echo htmlentities($res['start']); ?> 至 <?php echo htmlentities($res['end']); ?>
                                        </p>
                                        <p>班次号：<?php echo date('Y-m-d'); ?></p>
                                        <p>交班操作员：<?php echo session('admin'); ?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-col-sm6 layui-col-md3" style="border-right-style: dotted;">
                                <div class="layui-card">
                                    <div class="layui-card-header">
                                        <span class="layui-badge layui-bg-cyan layuiadmin-badge">本班账务汇总信息</span></div>
                                    <div class="layui-card-body">
                                        <p class="layuiadmin-big-font">
                                            订房：<?php echo htmlentities($res['home_subscribe_msg']); ?>元
                                           <span style="margin-left: 120px;">会员充值：<?php echo htmlentities($res['member_record']); ?>元</span>
                                        </p>
                                        <p>
                                            入住：<?php echo htmlentities($res['move']); ?>元
                                            <span style="margin-left: 120px;">商品消费：<?php echo htmlentities($res['goods_shop']); ?>元</span>
                                        </p>
                                        <!--<p>押金：<?php echo htmlentities($res['deposit_record']); ?>元</p>-->
                                        <p>总金额：<?php echo htmlentities($res['home_subscribe_msg'] + $res['member_record'] + $res['move'] + $res['goods_shop']); ?>元</p>
                                    </div>
                                </div>
                            </div>



                            <hr>
                            <div class="layui-col-md12">
                                <div class="layui-card">
                                    <div class="layui-card-header">房间信息</div>
                                    <div class="layui-card-body ">
                                        <table class="layui-table">
                                            <tbody>
                                            <tr>
                                                <th>房间入住数</th>
                                                <td><?php echo htmlentities($res['room_count']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>房间剩余数</th>
                                                <td><?php echo htmlentities($res['free_count']); ?></td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="img">
                            <button type="button" class="layui-btn layui-btn-fluid" onclick="Handover()">确认交班</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>

<script>

    //创建一个新的canvas
    new html2canvas(document.getElementById('seho')).then(canvas => {
        // canvas为转换后的Canvas对象
        let oImg = new Image();
        //oImg.src图片地址
        oImg.src = canvas.toDataURL("image/jpg");

//        document.body.appendChild(oImg);  // 将生成的图片添加到body
    console.log(oImg);
        console.log(oImg.src);
        $('#img').val(oImg.src);

    });

    function Handover(){
        $.ajax({
            type:"post",
            url: "<?php echo url('home/Handover/record'); ?>",
            data: {
                img: $('#img').val()
            },
            success: function(data){
                console.log(data);
                toastr.error(data.msg);
                if(data.code == 100){
                    setTimeout(function () {
                        layer.closeAll();
                        parent.location.reload();
                    },1500);
                }
            }});
    }
</script>


</html>