<?php /*a:3:{s:57:"G:\phpstudy_pro\WWW\tp\view\home\atrial\replace_room.html";i:1603363167;s:51:"G:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"G:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>

    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">演示</a>
                <a>
                    <cite>导航元素</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card" id="app">

                        <div class="layui-container">
                            <div class="layui-row">
                                <div class="layui-col-md4">


                                    <fieldset class="layui-elem-field layui-field-title">
                                        <legend id="room_num">房间号：<?php echo htmlentities($room['room_num']); ?></legend>
                                        <div class="layui-field-box">
                                            <input type="hidden" id="room_id" value="<?php echo htmlentities($room['id']); ?>">
                                            <table class="layui-table">
                                                <tbody>
                                                <tr>
                                                    <td>类型:</td>
                                                    <td><?php echo htmlentities($room['type_name']); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>价格</td>
                                                    <td><?php echo htmlentities($room['monday']); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>押金</td>
                                                    <td><?php echo htmlentities($room['deposit']); ?></td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </fieldset>

                                    <fieldset class="layui-elem-field layui-field-title" v-show='id'>
                                        <legend id="room_num">房间号：{{room_num}}</legend>
                                        <div class="layui-field-box">
                                            <input type="hidden" id="room_id" value="<?php echo htmlentities($room['id']); ?>">
                                            <table class="layui-table">
                                                <tbody>
                                                <tr>
                                                    <td>类型:</td>
                                                    <td>{{type_name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>价格</td>
                                                    <td>{{monday}}</td>
                                                </tr>
                                                <tr>
                                                    <td>押金</td>
                                                    <td>{{deposit}}</td>
                                                </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </fieldset>

                                </div>
                                <div class="layui-col-md7" style="margin-left: 50px;">
                                    <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>

                                        <button type="button"class="layui-btn layui-btn-lg layui-btn-normal"
                                                style="margin: 10px;"
                                                @click="replace(<?php echo htmlentities($vo['id']); ?>,<?php echo htmlentities($vo['room_num']); ?>,'<?php echo htmlentities($vo['type_name']); ?>',<?php echo htmlentities($vo['monday']); ?>,<?php echo htmlentities($vo['deposit']); ?>)">
                                            <?php echo htmlentities($vo['room_num']); ?>
                                        </button>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                            </div>

                            <button type="button" class="layui-btn layui-btn-lg" @click="updates()">
                                确定换房
                            </button>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </body>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    new 	Vue({
        el:'#app',
        data:{
            id:false,
            room_num:'',
            type_name:"",
            monday:'',
            deposit:'',
        },
        methods:{
            replace:function(id,num,type_name,monday,deposit){
                this.room_num = num;
                this.type_name = type_name;
                this.monday = monday;
                this.deposit = deposit;
                this.id = id;
            },
            updates:function () {
                $.ajax({
                    type:"post",
                    url: "<?php echo url('home/atrial/replace_room'); ?>",
                    data: {
                        room_num:this.room_num,
                        room_id:$('#room_id').val(),
                        replace_id:this.id
                    },
                    success: function(data){
                        console.log(data);
                        toastr.error(data.msg);
                        if(data.code == 100){
                            setTimeout(function () {
                                layer.closeAll();
                                parent.location.reload();
                            },500);
                        }
                    }});
            }
        }
    })
</script>


</html>