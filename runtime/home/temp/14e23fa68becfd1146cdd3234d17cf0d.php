<?php /*a:2:{s:50:"D:\phpstudy_pro\WWW\tp\view\home\welcome\move.html";i:1604887375;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<link href="/static/bootstrap3.4.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
    <body>
        <div class="x-nav">
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15" >
                <div class="layui-col-md12" >
                    <div class="layui-card">
                        <form action="<?php echo url('home/welcome/move'); ?>" method="post">
                        <div class="layui-card-body" id="app">

                                <ul class="layui-timeline">
                                    <li class="layui-timeline-item">
                                        <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                                        <div class="layui-timeline-content layui-text">
                                            <h3 class="layui-timeline-title">
                                                入住信息(房间：<?php echo htmlentities($room); ?>）
                                            </h3>
                                            <table >
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="layui-form-item">
                                                                <label for="username" class="layui-form-label">
                                                                    <span class="x-red">*姓名</span>
                                                                </label>
                                                                <div class="layui-input-inline">
                                                                    <input type="text" name="guest_name" required="" lay-verify="required"
                                                                           autocomplete="off" class="layui-input" id="guest_name">
                                                                    <input type="hidden" name="id" value="<?php echo htmlentities($id); ?>">
                                                                    <input type="hidden" name="room_num" value="<?php echo htmlentities($room); ?>">
                                                                    <input type="hidden" name="member_id" id="member_id">
                                                                </div>
                                                                <div class="layui-form-mid layui-word-aux">
                                                                    <span class="x-red">
                                                                        <!-- Button trigger modal -->
                                                                        <button type="button" class="layui-btn layui-btn-normal" data-toggle="modal" data-target="#myModal">
                                                                          会员
                                                                        </button>
                                                                        <!-- 模态框开始 -->
                                                                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                              <div class="modal-dialog" role="document">
                                                                                <div class="modal-content">
                                                                                  <div class="modal-header">
                                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                    <h4 class="modal-title" id="myModalLabel">会员</h4>
                                                                                  </div>
                                                                                  <div class="modal-body">

                                                                                             <table class="layui-table layui-form">
                                                                                                <thead>
                                                                                                <tr>
                                                                                                    <th>ID</th>
                                                                                                    <th>会员</th>
                                                                                                    <th>证据号码</th>
                                                                                                    <th>操作</th></tr>
                                                                                                </thead>
                                                                                                <tbody>
                                                                                                <?php if(is_array($member) || $member instanceof \think\Collection || $member instanceof \think\Paginator): $i = 0; $__LIST__ = $member;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$m): $mod = ($i % 2 );++$i;?>
                                                                                                <tr>
                                                                                                    <td><?php echo htmlentities($m['id']); ?></td>
                                                                                                    <td><?php echo htmlentities($m['name']); ?></td>
                                                                                                    <td><?php echo htmlentities($m['identity']); ?></td>
                                                                                                    <td class="td-manage">
                                                                                                        <a title="查看" @click="member(<?php echo htmlentities($m['id']); ?>)" href="javascript:;">
                                                                                                            <i class="layui-icon">&#xe63c;</i>选择
                                                                                                        </a>
                                                                                                    </td>
                                                                                                </tr>
                                                                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                                                                                </tbody>
                                                                                            </table>

                                                                                  </div>
                                                                                  <div class="modal-footer">
                                                                                    <button type="button" class="btn  btn-primary" data-dismiss="modal">保存</button>

                                                                                  </div>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                        <!-- 模态框开始 -->
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="layui-form layui-form-item">
                                                                <label class="layui-form-label" style="width: 100px;">优惠活动</label>
                                                                <div class="layui-input-block">
                                                                    <select name="activity_id" lay-verify="required">
                                                                        <option value=''>请选择</option>
                                                                        <?php if(is_array($activity) || $activity instanceof \think\Collection || $activity instanceof \think\Paginator): $i = 0; $__LIST__ = $activity;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                                                            <option value=<?php echo htmlentities($vo['id']); ?>><?php echo htmlentities($vo['name']); ?></option>
                                                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="layui-form-item">
                                                                <label class="layui-form-label" style="width: 100px;">
                                                                    <span class="x-red">证件号码</span>
                                                                </label>
                                                                <div class="layui-input-block">
                                                                    <input type="text" name="credentials" required="" lay-verify="required" id="credentials"
                                                                           autocomplete="off" class="layui-input">
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="layui-form-item">
                                                                <label class="layui-form-label" style="width: 100px;">
                                                                    <span class="x-red" >手机号码</span>
                                                                </label>
                                                                <div class="layui-input-inline">
                                                                    <input type="text" name="guest_tel" id="guest_tel" required="" lay-verify="required"
                                                                           autocomplete="off" class="layui-input">
                                                                </div>
                                                                <div class="layui-form-mid layui-word-aux">
                                                                    <span class="x-red">*</span>将会成为您
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="layui-form layui-form-item">
                                                                <label class="layui-form-label">性别</label>
                                                                <div class="layui-input-block">
                                                                    <select name="guest_sex" lay-verify="required">
                                                                        <option value="0">男</option>
                                                                        <option value="1">女</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="layui-form layui-form-item">
                                                                <label class="layui-form-label" style="width: 100px;">客户来源</label>
                                                                <div class="layui-input-block">
                                                                    <select name="guest_source" lay-verify="required">
                                                                        <?php if(is_array($guest) || $guest instanceof \think\Collection || $guest instanceof \think\Paginator): $i = 0; $__LIST__ = $guest;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vv): $mod = ($i % 2 );++$i;?>
                                                                            <option value=<?php echo htmlentities($vv['id']); ?>><?php echo htmlentities($vv['guest']); ?></option>
                                                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <blockquote class="layui-elem-quote layui-quote-nm">
                                                                {{room_types}}
                                                                <button type="button" class="layui-btn layui-btn-normal" @click="room_type()">
                                                                    切换
                                                                </button>
                                                            </blockquote>
                                                            <div class="layui-form layui-col-space5"  v-if="room_types == '全日房'">
                                                                <div class="layui-input-inline layui-show-xs-block">
                                                                    <input class="layui-input" placeholder="开始日" name="in_time" id="start">
                                                                </div>
                                                                <div class="layui-input-inline layui-show-xs-block">
                                                                    <input class="layui-input" placeholder="截止日" name="move_time" id="end">
                                                                </div>
                                                            </div>
                                                            <div class="layui-form layui-col-space5"  v-else>
                                                                <div class="layui-input-inline">
                                                                    <input type="text" name="hours" placeholder="请输入需要入住/小时"
                                                                           autocomplete="off" class="layui-input">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="layui-form layui-form-item">
                                                                <label class="layui-form-label" style="width: 100px;">支付方式</label>
                                                                <div class="layui-input-block">
                                                                    <select name="payment_id" lay-verify="required">
                                                                        <?php if(is_array($payment) || $payment instanceof \think\Collection || $payment instanceof \think\Paginator): $i = 0; $__LIST__ = $payment;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$pay): $mod = ($i % 2 );++$i;?>
                                                                        <option value=<?php echo htmlentities($pay['id']); ?>><?php echo htmlentities($pay['pay_name']); ?></option>
                                                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </li>
                                    <!--房间信息开始-->
                                    <li class="layui-timeline-item">
                                        <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                                        <div class="layui-timeline-content layui-text">
                                            <h3 class="layui-timeline-title">房间信息</h3>
                                            <button type="button" class="layui-btn layui-btn-normal" @click="add(<?php echo htmlentities($room); ?>)">
                                                添加入住
                                            </button>
                                            <!-- Large modal -->
                                            <button type="button" class="layui-btn layui-btn-normal" data-toggle="modal"  data-target=".bs-example-modal-lg">选择房间</button>

                                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content" style="padding: 10px;">
                                                        <fieldset class="layui-elem-field layui-field-title">
                                                            <legend>房间信息</legend>
                                                            <div class="layui-field-box">

                                                                <div class="layui-form-item layui-form-text">
                                                                    <label class="layui-form-label">文本域</label>
                                                                    <div class="layui-input-block">
                                                                        <textarea name="room_arr" id="label" placeholder="请输入内容" class="layui-textarea"></textarea>
                                                                    </div>
                                                                </div>

                                                                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                                                                    <button type="button" name="<?php echo htmlentities($vo['room_num']); ?>"
                                                                            class="layui-btn layui-btn-normal"
                                                                            style="width: 100px;height: 50px;margin: 10px;"
                                                                            @click="add_label(<?php echo htmlentities($vo['room_num']); ?>,<?php echo htmlentities($vo['id']); ?>)">
                                                                        <?php echo htmlentities($vo['room_num']); ?>
                                                                        <?php echo htmlentities($vo['type_name']); ?>
                                                                    </button>
                                                                <?php endforeach; endif; else: echo "" ;endif; ?>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">保存</button>
<!--                                                        <button type="button" class="btn btn-primary">Save changes</button>-->
                                                    </div>
                                                </div>
                                            </div>
                                            <!--模态框结束-->

                                            <div class="layui-row">

                                                <div class="layui-col-md3" style="border: 1px solid #e6e6e6;margin: 10px;" v-for='item in arr' :id='item'>
                                                    <blockquote class="layui-elem-quote layui-quote-nm">
                                                        房间号：{{item}}
                                                        <button type="button" class="layui-btn" @click="add_people(item)">添加入住人</button>
                                                    </blockquote>
                                                    <div class="layui-form layui-col-space5">
                                                        <div class="layui-input-inline layui-show-xs-block">
                                                            <input type="text" name="user_name[]" placeholder="入住者姓名" autocomplete="off" required="" class="layui-input" lay-verify="required" style="width: 120px;">
                                                        </div>
                                                        <div class="layui-input-inline layui-show-xs-block">
                                                            <input type="text" name="user_num[]" placeholder="证据号码" autocomplete="off" required="" class="layui-input" lay-verify="required">
                                                        </div>
                                                        <input type="hidden" name="room_id[]" :value="item">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </li>
                                    <!--房间信息结束-->
                                </ul>
                            <button type="submit" class="layui-btn layui-btn-normal" >
                                下一步
                            </button>
                        </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </body>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script>
    layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;
            //执行一个laydate实例
            laydate.render({
                elem: '#start' //指定元素
                ,value: new Date()
            });
            //执行一个laydate实例
            laydate.render({
                elem: '#end' //指定元素
                ,value: new Date(new Date().getTime() + 24*60*60*1000)
            });
        });
new 	Vue({
    el:'#app',
    data:{
        arr:[],
        num:1000,
        show:false,
        room_types:'全日房',
    },
    methods:{
        //选择房间号追加的表单
        add_label:function(num,id){
            //检测数据是否 重复添加
            var data = $.inArray(num,this.arr);
            console.log(data);
            if(data == '-1'){
                this.arr.push(num);
                $('#label').val(this.arr);
                console.log($('#label').val());
                $( "button[name="+num+"]").removeClass("layui-btn-normal").addClass("layui-btn-danger");
            }else{
                //再次点击移除数据
                this.arr.splice($.inArray(num,this.arr),1);
                $('#label').val(this.arr);
                console.log($('#label').val());
                $( "button[name="+num+"]").removeClass("layui-btn-danger").addClass("layui-btn-normal");
            }
        },//添加入住人
        add_people:function (id) {
            this.num = this.num+10;
            var text = "                                            <div class=\"layui-form layui-col-space5\" id=\""+this.num+"\">\n" +
                "                                                        <div class=\"layui-input-inline layui-show-xs-block\">\n" +
                "                                                            <input type=\"text\" name=\"user_name[]\" placeholder=\"入住者姓名\" required=\"\" lay-verify=\"required\" autocomplete=\"off\" class=\"layui-input\" style=\"width: 120px;\">\n" +
                "                                                        </div>\n" +
                "                                                        <div class=\"layui-input-inline layui-show-xs-block\">\n" +
                "                                                            <input type=\"text\" name=\"user_num[]\" placeholder=\"证据号码\" required=\"\" lay-verify=\"required\" autocomplete=\"off\" class=\"layui-input\">\n" +
                "                                                        </div>\n" +
                "                                                        <a type=\"button\" class=\"layui-btn\" onclick=\"del_people("+this.num+")\">删除</a>\n" +
                "                                                    <input type=\"hidden\" name=\"room_id[]\" value=\""+id+"\">" +
                "                                                   </div> ";
            $('#'+id+'').append(text);
        },
        //添加住房间的人住人数
        add:function (data) {
            if(this.show == false){
                this.arr = [data];
                this.show = true;
                $('#label').val(data);
            }else {
                this.arr = [];
                this.show = false;
            }
        },
        //房间类型
        room_type:function () {
            if(this.room_types == '钟点房'){
                this.room_types = '全日房';
            }else {
                this.room_types = '钟点房';
            }
        },//会员选择
        member:function (id) {
            $.ajax({
                type:"post",
                url: "<?php echo url('home/welcome/select_member'); ?>",
                data: {
                    id:id,
                },
                success: function(data){
                    console.log(data);
                    console.log(data.name);
                    $('#guest_name').val(data.name);
                    $('#credentials').val(data.identity);
                    $('#guest_tel').val(data.tel);
                    $('#member_id').val(data.id);
                    // $('#myModal').hide();
                }});
        }
    }
})

    //删除入住的人物表单
    function del_people(id) {
        $('#'+id+'').remove();
    }
</script>


</html>