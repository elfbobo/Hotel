<?php
namespace app\api\controller;

use think\facade\Db;

class HotelOreder extends Super
{

    /*
     * app 首页面
     * */
    public function index(){
        if(request()->isPost()) {
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }
            $user = Db::table('app_member')->where('token',input('token'))->find();

//            $list = Db::table('app_subscribe_order')->where('user_id',$user['id'])->paginate(10);

            if(!empty(input('page'))){
                $page = input('page') + 10;
            }else{
                $page = 10;
            }

            $list = Db::table('app_subscribe_order')
                        ->alias('a')
                        ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building')
                        ->join('layout b','a.layout_id = b.id')
                        ->join('building c','a.building_id = c.id')
                        ->where('a.user_id',$user['id'])
//                        ->paginate($page);
                        ->limit(0,$page)->select();
            return json([
                'msg' => $list,
                'code' => '200',
                'page'=>$page
            ]);
        }
    }

    /*
     * 查询单个订单
     * */
    public function find_order(){
        if(request()->isPost()){
            if(!is_array($this->check_token(input('token')))){
                return $this->return_json('令牌错误','0');
            }

            $list = Db::table('app_subscribe_order')
                ->alias('a')
                ->field('a.*,b.type_name,b.price,b.deposit,b.hour,c.building,d.address')
                ->join('layout b','a.layout_id = b.id')
                ->join('building c','a.building_id = c.id')
                ->join('hotel_system d','a.building_id = d.building_id')
                ->where('a.id',input('id'))
                ->find();
            return json([
                'msg' => $list,
                'code' => '200'
            ]);

        }
    }


}
