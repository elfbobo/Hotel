<?php
namespace app\api\controller;

use app\BaseController;
use think\facade\Db;

class Index extends BaseController
{
    //主页额查询房间
    public function index()
    {
        if(request()->isPost()){
            if(!empty(input('page'))){
                $num = (int)input('page');
                $page = $num + 20;
            }else{
                $page = 19;
            }
            $res = $this->check_token(input('user_token'));

            $map = [
                ['a.admin','=',$res['id']],
                ['a.status','=','1'],
            ];
            $list = Db::table('home_room_publish')
                        ->alias('a')
                        ->field('a.*,b.room_num,c.name,d.service_name')
                        ->join('room b','a.room = b.id')
                        ->join('home_room_admin c','a.admin = c.id')
                        ->join('home_room_service d','a.service = d.id')
                        ->where($map)
                        ->limit(0,$page)->select();
            return json([
                'data' => $list,
                'code' => '200',
                'page' => $page,
            ]);
        }
    }

    /*
     * 查询单个房间
     * */
    public function find_room(){
        if(request()->isPost())
        {
            $data =  input('param.');
            $this->check_token(input('user_token'));
            $map = [
                ['a.id','=',$data['id']],
                ['b.room_num','=',$data['room_num']],
            ];
            $list = Db::table('home_room_publish')
                ->alias('a')
                ->field('a.*,b.room_num,b.guest_name,b.guest_tel,c.name,d.service_name,e.type_name')
                ->join('room b','a.room = b.id')
                ->join('home_room_admin c','a.admin = c.id')
                ->join('home_room_service d','a.service = d.id')
                ->join('layout e','b.type_id = e.id')
                ->where($map)
                ->find();
            return json([
                'data' => $list,
                'code' => '200',
            ]);
        }
    }

    /*
     * 开始工作
     * */
    public function start_work(){
        $data =  input('param.');
        $this->check_token(input('user_token'));
        $list = Db::table('home_room_publish')->where('id',$data['id'])->update(['status'=>'2']);
        if($list){
            return json([
                'msg' => '已经开始工作',
                'code' => '200',
            ]);
        }else{
            return json([
                'msg' => '工作失败',
                'code' => '0',
            ]);
        }
    }

    /*
     * 完成任务
     * */
    public function accomplish(){
        $data =  input('param.');
        $this->check_token(input('user_token'));
        $list = Db::table('home_room_publish')->where('id',$data['id'])->update(['status'=>'3']);
        if($list){
            return json([
                'msg' => '工作完成',
                'code' => '200',
            ]);
        }else{
            return json([
                'msg' => '工作未完成',
                'code' => '0',
            ]);
        }
    }

    /*
     * 查询任务状态
     * */
    public function room_status(){
        $this->check_token(input('user_token'));
        $data = Db::table('home_room_publish')
            ->alias('a')
            ->field('a.*,b.room_num,b.guest_name,b.guest_tel,c.name,d.service_name,e.type_name')
            ->join('room b','a.room = b.id')
            ->join('home_room_admin c','a.admin = c.id')
            ->join('home_room_service d','a.service = d.id')
            ->join('layout e','b.type_id = e.id')
            ->where('a.status','2')
            ->select();
        $list = Db::table('home_room_publish')
            ->alias('a')
            ->field('a.*,b.room_num,b.guest_name,b.guest_tel,c.name,d.service_name,e.type_name')
            ->join('room b','a.room = b.id')
            ->join('home_room_admin c','a.admin = c.id')
            ->join('home_room_service d','a.service = d.id')
            ->join('layout e','b.type_id = e.id')
            ->where('a.status','3')
            ->select();

        return json([
            'data' => $data,
            'list' => $list,
            'code' => '200',
        ]);
    }

    /*
     * 检查token
     * */

    public function check_token($token){
        $res = Db::table('home_room_admin')->where('token',$token)->find();
/*        dump($res);
        dump(Db::table('home_room_admin')->getLastSql());*/
        if(!is_array($res)){
            return 'error';
        }else{
            return $res;
        }
    }

}
