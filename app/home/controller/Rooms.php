<?php
namespace app\home\controller;

use app\BaseController;
use app\index\controller\Basics;
use app\index\validate\Room;
use think\facade\Db;

/*
 * 房间信息
 *
 * */

class Rooms extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        $this->model_name = 'Room';
        $this->new_model();
        $this->validate = new Room();
        parent::initialize();
    }

    /*
     * 房间首页
     * */
    public function index()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['type_id'])){
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.type_id','=',$data['type_id']],
                ];
            }
            if(!empty($data['name'])){
                $name = $data['name']."%";
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.room_num','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list = Db::table('room')
                ->alias('a')
                ->field('a.*,b.building,c.storey,d.type_name,d.price,d.deposit,d.hour,e.monday')
                ->join('building b','a.building_id = b.id')
                ->join('storey c','a.storey_id = c.id')
                ->join('layout d','a.type_id = d.id')
                ->join('week e','a.id = e.layout_id')
                ->where($map)
                ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        $layout =  Db::table('layout')->where('building_id',session('building_id'))->select();
        return view('index',['list' => $list,'layout'=>$layout]);
    }

    /*
     * 添加房间
     * */
    public function adds(){
        if(request()->isAjax()){
            $data = input('param.');
            //验证字段
            if(!$this->checkDate(input('param.'))){
                return $this->return_json($this->validate->getError(),'0');
            }
            //添加数据
//            return $this->model->add_plus();
            $data['create_time'] = time();
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(Db::name('room')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        $storey = Db::name('storey')->where('building',session('building_id'))->select();
        $layout =  Db::table('layout')->where('building_id',session('building_id'))->select();
        return view('adds',['storey' => $storey,'layout' => $layout]);
    }

    /*
     * 编辑房间
     * */
    public function edits(){
//        $list = $this->select_find('layout',['id' => input('id')]);
        $list = $this->model->find_room(['a.id' => input('id')]);
//        dump($list);
        if(request()->isAjax()){
            //编辑数据
            return $this->model->edit_plus();
        }
        $storey = Db::name('storey')->where('building',session('building_id'))->select();
        $layout =  Db::table('layout')->where('building_id',session('building_id'))->select();
        return view('edits',['storey' => $storey,'layout' => $layout,'list' => $list]);
    }

}
