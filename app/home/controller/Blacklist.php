<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;
/*
 * 黑名单管理
 * */
class Blacklist extends Basics
{
    /*
     * 黑名单
     * */
    public function index()
    {
        if(request()->isGet()){
            $data = input('param.');

            if(!empty($data['name'])){
                $name = '%'.$data['name']."%";
                $map = [
                    ['building_id','=',session('building_id')],
                    ['name','like',$name],
                ];
            }

            if(!empty($data['identity'])){
                $identity = '%'.$data['identity']."%";
                $map = [
                    ['building_id','=',session('building_id')],
                    ['identity','like',$identity],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['building_id','=',session('building_id')]
            ];
        }
        $list = Db::name('blacklist')
            ->where($map)
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        return view('index',['list' => $list]);
    }

    /*
     * 添加黑名单
     * */
    public function adds()
    {
        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(Db::name('blacklist')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        return view();
    }

    /*
     * 删除黑名单
     * */
    public function deletes()
    {
        if(request()->isAjax()){
            if(Db::name('blacklist')->where('id',input('id'))->delete()){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
    }

}
