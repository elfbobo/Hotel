<?php
namespace app\home\controller;

use app\index\controller\Basics;
use think\facade\Db;
/*
 * 营业查询
 * */
class Income extends Basics
{
    /*
     * 入住记录
     * */
    public function index()
    {
        if(request()->isGet()){

            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
            if(!empty($data['status'])){
                if($data['status'] == 2){
                    $map = [
                        ['a.building_id','=',session('building_id')],
                        ['a.status','=','0'],
                    ];
                }else{
                    $map = [
                        ['a.building_id','=',session('building_id')],
                        ['a.status','=',$data['status']],
                    ];
                }

            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list = Db::table('home_room_income')
                ->alias('a')
                ->field('a.*,b.username')
                ->join('admin b','a.operator = b.id')
                ->where($map)
                ->order('create_time', 'desc')
                ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        //根据房间查询多房间的房间号
        foreach ($list as $k=>$v){
            //查询房间
            $DATA = Db::table('room')->where('id','IN',$v['room_arr'])->select();
            $num_arr = [];
            //提取房间号
            foreach ($DATA as $datas){
                array_push( $num_arr,$datas['room_num']);
            }
            //房间号更新到数据库
            Db::table('home_room_income')
                ->where('id',$v['id'])->update(['num_arr'=>implode(",", $num_arr)]);
        }
        dump($list);
        return view('index',['list' => $list]);
    }

    /*
     * 预订记录
     * */
    public function subscribe()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['b.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
            if(!empty($data['name'])){
                $name = '%'.$data['name']."%";
                $map = [
                    ['b.building_id','=',session('building_id')],
                    ['b.room_num','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['b.building_id','=',session('building_id')]
            ];
        }

        $list =  Db::table('home_subscribe_msg')
            ->alias('a')
            ->field('a.*,b.room_num,c.username')
            ->join('room b','a.room_id = b.id')
            ->join('admin c','a.operator = c.id')
            ->where($map)
            ->order('create_time', 'asc')
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);

        return view('subscribe',['list' => $list]);
    }

    /*
     * 换房记录
     * */
    public function upd_rooms()
    {
        if(request()->isGet()){
            $data = input('param.');

            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['a.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['a.building_id','=',session('building_id')]
            ];
        }
        $list =  Db::table('home_room_records')
            ->alias('a')
            ->field('a.*,b.username')
            ->join('admin b','a.operator = b.id')
            ->where($map)
            ->order('create_time', 'asc')
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        return view('upd_rooms',['list' => $list]);
    }

    /*
     * 商品消费记录
     * */
    public function goods_shop()
    {
        if(request()->isGet()){
            $data = input('param.');
            if(!empty($data['start']) && !empty($data['end'])){
                $data['start'] = strtotime($data['start']);
                $data['end'] = strtotime($data['end']);
                $map = [
                    ['d.building_id','=',session('building_id')],
                    ['a.create_time','>',$data['start']],
                    ['a.create_time','<',$data['end']]
                ];
            }
            if(!empty($data['name'])){
                $name = '%'.$data['name']."%";
                $map = [
                    ['d.building_id','=',session('building_id')],
                    ['b.room_num','like',$name],
                ];
            }
        }
        if(!isset($map)){
            $map = [
                ['b.building_id','=',session('building_id')]
            ];
        }

        $data =  Db::table('consume')
            ->alias('a')
            ->field('a.*,b.room_num,d.number,d.name,d.price,e.username')
            ->join('room b','a.room_id = b.id')
            ->join('goodss d','a.goods_id = d.id')
            ->join('admin e','a.operator = e.id')
            ->where($map)
            ->order('a.create_time', 'asc')
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);

        return view('goods_shop',['data' => $data]);
    }

    /*
     * 退费记录
     * */
    public function room_return()
    {
        $data =  Db::table('home_room_refund')
            ->alias('a')
            ->field('a.*,b.room_num,e.username')
            ->join('room b','a.room_id = b.id')
            ->join('admin e','a.operator = e.id')
            ->where('a.building_id',session('building_id'))
            ->order('a.create_time', 'asc')
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        dump($data);
        return view('room_return',['list'=>$data]);
    }


}
