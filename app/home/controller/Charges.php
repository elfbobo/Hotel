<?php
namespace app\home\controller;


/*
 * 收费方式
 *
 * */

use app\index\controller\Basics;

class Charges extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        $this->model_name = 'Charge';
        $this->new_model();
        parent::initialize();
    }

    /*
     * 普通用户
     * */
    public function index()
    {
        $list = $this->select_find('charge',['username' => 'ordinary','building_id'=>session('building_id')]);
        return view('index',['list' => $list]);
    }

    /*
     * 酒店会员
     * */
    public function vip(){
        $list = $this->select_find('charge',['username' => 'vip','building_id'=>session('building_id')]);
        return view('index',['list' => $list]);
    }


}
