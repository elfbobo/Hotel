<?php
declare (strict_types = 1);

namespace app\index\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Layout extends Base
{
    /*
     * 添加数据
     * */
    public function  add_plus(){

        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['building_id'] = session('building_id');
            //判断是否添加成功
            if(self::save($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
    }

}
